# GReenlandIcebergDistribution(GRID)

## Overview:
Icebergs are an important output of the calving process that provides insights into the freshwater intrusion from glaciers into the ocean. 
Having a continuous detection and tracking of icebergs in fjords and ocean has lead to the usage of satellite images. 
Considering the extreme conditions of the polar regions, the analysis of icebergs becomes a complex task. Here we use an iterative CFAR technique to identify icebergs in polar conditions. 
The technique localizes the identification of icebergs to the size of sliding window and extends to 150 km of coastal waters around the Greenland Ice Sheet.


## Code: 
The main code is in the GrID_py3.py file. This code shows the complete methodology implemented on the coastal waters of the Greenland Ice Sheet.

## Output:
The output is a .csv and .tif generated from the code. The csv file includes coordinates and area of icebergs identified in the detection process.

![Coverage](https://bitbucket.org/sid07/greenlandicebergdistribution-grid/raw/71c2d6c1b7b9c2a9fea5553a4d9de46d082764d2/img/mosaic_greenland_updated-crop2.png)
![Sermilik Fjord](https://bitbucket.org/sid07/greenlandicebergdistribution-grid/raw/71c2d6c1b7b9c2a9fea5553a4d9de46d082764d2/img/icebergs.png)
![Subset 1](https://bitbucket.org/sid07/greenlandicebergdistribution-grid/raw/71c2d6c1b7b9c2a9fea5553a4d9de46d082764d2/img/subset1.png)
![Subset 2](https://bitbucket.org/sid07/greenlandicebergdistribution-grid/raw/71c2d6c1b7b9c2a9fea5553a4d9de46d082764d2/img/subset2.png)

