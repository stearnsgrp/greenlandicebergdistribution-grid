# -*- coding: utf-8 -*-

"""
The MIT License

Copyright (c) 2019 Siddharth Shankar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 


Algorithm: Pre and Post processing along with excerpts from Frost et al, 2016, Canadian Journal of Remote Sensing
"""





'''
#########################
IMPORT LIBRARIES
#########################
'''

import gdal
import numpy as np
from matplotlib import pyplot as plt
import skimage
from skimage import measure
import csv
import time
import skimage.morphology
from skimage import segmentation
import pandas as pd
import os
from fnmatch import fnmatch
from scipy import ndimage as ndi
import itertools as itr #use to replace for loop
from pyproj import Proj, transform
import rasterio as ras
from shapely.geometry import Point, MultiPoint
from shapely.geometry import mapping
import fiona
from fiona.crs import from_epsg
from fiona import collection
import geopandas as gpd
from collections import OrderedDict
from scipy import ndimage
from skimage.feature import blob_doh,blob_dog,blob_log
from math import sqrt
from rasterio import windows
from itertools import product
import timeit
#import cv2
from skimage.color import rgb2gray
from tqdm import tqdm
import math
from multiprocessing import cpu_count,Pool
import geopandas as gpd
from skimage import exposure
from rasterio.windows import Window
import cv2

'''
#########################
INPUT USER DATA
#########################
'''
root2 = r'C:\Research\chunk_read/Results/'
out_path = r'C:\Research\chunk_read\Results/'
pattern = "*.tif" 
output_filename = 'proc_tile_{}.tif'

tile_name = 'tile_500-500'
fileName = root2+tile_name+'.tif'
print (fileName)

'''
#########################
PROCESSING RASTER
#########################
'''

#for sar in fileName:

#Time setup
startTime = time.time()
   
sar_img = fileName

read_sar = ras.open(sar_img)
cols = read_sar.width #number of cols --need to test
rows = read_sar.height#number of rows --need to test
print ("rows",rows)
print ("cols",cols)

transform = read_sar.transform
proj = (read_sar.crs)

print(read_sar.profile)

print (transform)


meta = read_sar.meta.copy()

img_2Darray = read_sar.read(1)
print(img_2Darray)

if np.count_nonzero(img_2Darray)==0:
    print ("Skipping Image tile:"+tile_name+"- has less than 100 data points!")
else:           
    print(img_2Darray.shape)
        
    row_pad,col_pad = img_2Darray.shape
    print ("col_pad: ",col_pad)
    print ("row_pad: ",row_pad)
    print((transform)*(col_pad,row_pad))

    binary_img = np.zeros((row_pad,col_pad))
    #Convert to pandas dataframe    
    df = pd.DataFrame(img_2Darray)
    
    #Create a binary image for outputting the image.
    binary_img_lg = np.zeros((row_pad,col_pad))
    binary_img_sm = np.zeros((row_pad,col_pad))
    binary_img = np.zeros((row_pad,col_pad))
    
    '''
    First Iteration for detecting large icebergs > 1000px
    N-sigma code
    '''
    
    a=10 #10
    b =5 #2.4
    loop =4
    win = 299
    for z in tqdm(range(loop)):
        print ("Iteration large iceberg = ",z+1)

        roll_mean = df.rolling(win,min_periods=50,center=True).mean() #original
        roll_std = df.rolling(win,min_periods=50,center=True).std() #original

        maxM = np.max(img_2Darray)
        minM =  np.min(img_2Darray)
       
        '''
        Threshold n=10 have pixels as seed
        Region growing on each of them and if the connected pixel > Threshold n = 5, mark it as seed and continue
        Mask all the seed regions and add them to binary image
        Iterate
        
        '''
           
        #   Calculating the primary threshold T
#        T = roll_mean + a*roll_std
        T = roll_mean.add(roll_std.mul(a))
        
        #   Index where the image pixel value is greater than threshold T - i.e Seed Region
        isDetected = (img_2Darray > T) # Index location of iceberg pixels in each iteration
        
        #   Calculating the secondary threshold T
#        Tb = roll_mean + (b*roll_std)
        Tb = roll_mean.add(roll_std.mul(b))
        
        marker = np.zeros_like(img_2Darray)
        marker[img_2Darray<=Tb] = 1
        marker[img_2Darray>Tb] = 2

        img_ws = skimage.morphology.watershed(isDetected,marker)  

        img_ws_ind  = (img_ws == 2) #Boolean Array with in
#        print (img_ws_ind)
        
        binary_img_lg[img_ws_ind] = 1
        
#        binary_img = ndi.binary_fill_holes(binary_img)
          
        img_2Darray[img_2Darray > Tb]  = np.nan#minM#0
        img_2Darray[isDetected] = np.nan#minM#0
        
        print ("Time to execute = %s sec" % (time.time() - startTime))
    
    binary_img_lg = ndi.binary_fill_holes(binary_img_lg)
    arr_lg = np.ma.array(binary_img_lg) #Binary Image
    
    
    '''
    ####################################################
    LABELING ALL 4 CONNECTED PIXELS AS 1 ICEBERG (LARGE)
    ####################################################
    '''
    all_labels,num_feat = skimage.measure.label(binary_img,background = 0,return_num = True, connectivity = 1) #Binary Image 
    print ("total icebergs",num_feat)
    properties_lg = measure.regionprops(all_labels)

    Area = [] 
    Area300 = []
    icePixelCoords = []
    total_area = 0
    
    '''
    ############################################################
    ACCESSING EACH 4 CONNECTED REGIONS (LARGE ICEBERGS) AS SINGLE UNIT
    ############################################################
    '''
    for prop in properties_lg:
        
        area = (prop.area*100) #10m X 10m spatial resolution: prop.area = no.of pixels
        coordinates = (prop.coords)
        
#                    centroid = prop.centroid
#                    print("centroid",centroid)
#                    
#                    xp, yp = ((read_sar.transform)*(centroid[0],centroid[1]))
        if (area > 100000):
            for coord in coordinates: #Get all pixel coordinates that are part of 1 iceberg region
                x,y = (transform)*(coord[0],coord[1])
                
                img_2Darray[coord[0],coord[1]] = 0 #Mask Large Icebergs

                binary_img_lg[coord[0],coord[1]] = 1 #Add Large Icebergs in Binary Image
                
            Area300.append(area)
            total_area+=area
        
        elif (area <= 300):
            
            for coord in coordinates:
                x,y = (transform)*(coord[0],coord[1])
                binary_img_lg[coord[0],coord[1]] = 0  #Mask icebergs in binary image less than 20 px to 0
    
    print ("Iceberg total area",np.sum(Area300))
    print("Total area",total_area)
#            np.savetxt("area.csv",Area300)
    plt.subplot(122),plt.imshow(binary_img_lg,cmap = 'hot'),plt.title("Icebergs detected > 1000 px")
    plt.xticks([]),plt.yticks([])
    plt.show()

    plt.subplot(122),plt.imshow(img_2Darray,cmap = 'hot'),plt.title("Binary_MaskedLargeBergs")
    plt.xticks([]),plt.yticks([])
    plt.show()
    
    '''
    First Iteration for detecting large icebergs <= 1000px
    '''
    
    a=10 #10
    b =2.4 #2.4
    loop =4
    win = 299
    for z in tqdm(range(loop)):
        print ("Iteration small iceberg = ",z)

        roll_mean = df.rolling(win,min_periods=50,center=True).mean() #original

        roll_std = df.rolling(win,min_periods=50,center=True).std() #original

        maxM = np.max(img_2Darray)
        minM =  np.min(img_2Darray)
       
        '''
        Threshold n=10 have pixels as seed
        Region growing on each of them and if the connected pixel > Threshold n = 5, mark it as seed and continue
        Mask all the seed regions and add them to binary image
        Iterate
        '''
        #   Calculating the primary threshold T
        T = roll_mean.add(roll_std.mul(a))
        
        #   Index where the image pixel value is greater than threshold T - i.e Seed Region
        isDetected = (img_2Darray > T) # Index location of iceberg pixels in each iteration
        
        #   Calculating the secondary threshold T
        Tb = roll_mean.add(roll_std.mul(b))

        marker = np.zeros_like(img_2Darray)
        marker[img_2Darray<=Tb] = 1
        marker[img_2Darray>Tb] = 2

        img_ws = skimage.morphology.watershed(isDetected,marker)  

        img_ws_ind  = (img_ws == 2) #Boolean Array with in
#        print (img_ws_ind)
        
        binary_img_sm[img_ws_ind] = 1
        
#        binary_img = ndi.binary_fill_holes(binary_img)
          
        img_2Darray[img_2Darray > Tb]  = np.nan#minM#0
        img_2Darray[isDetected] = np.nan#minM#0
        
        print ("Time to execute = %s sec" % (time.time() - startTime))
    
    binary_img_sm = ndi.binary_fill_holes(binary_img_sm)
    arr_sm = np.ma.array(binary_img_sm) #Binary Image
    
    '''
    ####################################################
    LABELING ALL 4 CONNECTED PIXELS AS 1 ICEBERG (SMALL)
    ####################################################
    '''
    
    all_labels,num_feat = skimage.measure.label(arr_sm,background = 0,return_num = True, connectivity = 1) #Binary Image 
    print ("total icebergs",num_feat)
    properties_sm = measure.regionprops(all_labels)
    Area = [] 
    Area300 = []
    icePixelCoords = []
    total_area = 0
    '''
    ############################################################
    ACCESSING EACH 4 CONNECTED REGIONS (SMALL ICEBERGS) AS SINGLE UNIT
    ############################################################
    '''
    for prop in properties_sm:
        
        area = (prop.area*100) #10m X 10m spatial resolution: prop.area = no.of pixels
        coordinates = (prop.coords)
        
#                    centroid = prop.centroid
#                    print("centroid",centroid)
#                    
#                    xp, yp = ((read_sar.transform)*(centroid[0],centroid[1]))
        if (area <= 100000 and area >300):
            for coord in coordinates: #Get all pixel coordinates that are part of 1 iceberg region
                x,y = (transform)*(coord[0],coord[1])
                
                img_2Darray[coord[0],coord[1]] = 0 #Mask Large Icebergs

                binary_img_lg[coord[0],coord[1]] = 1 #Add Large Icebergs in Binary Image
                
            Area300.append(area)
            total_area+=area
        
        elif (area <= 300):
            
            for coord in coordinates:
                x,y = (transform)*(coord[0],coord[1])
                binary_img_sm[coord[0],coord[1]] = 0  #Mask icebergs in binary image less than 20 px to 0
    
    print ("Iceberg total area",np.sum(Area300))
    print("Total area",total_area)
#            np.savetxt("area.csv",Area300)
    plt.subplot(122),plt.imshow(binary_img_sm,cmap = 'hot'),plt.title("Icebergs detected <= 1000 px")
    plt.xticks([]),plt.yticks([])
    plt.show()

    plt.subplot(122),plt.imshow(img_2Darray,cmap = 'hot'),plt.title("Binary_MaskedLargeBergs")
    plt.xticks([]),plt.yticks([])
    plt.show()
    
    #Merging two binary arrays    
#    binary_img = [map(sum, zip(*sum_)) for sum_ in zip(binary_img_lg,binary_img_sm)] 
    binary_img[binary_img_lg==1] = 1
    binary_img[binary_img_sm==1] = 1
#                binary_img
    arr = np.ma.array(binary_img)
    
#    print(binary_img.shape)
    


    '''
    ####################################################
    ADDING ALL ICEBERGS(LARGE & SMALL) TO CSV FILE
    ####################################################
    '''

    all_labels,num_feat = skimage.measure.label(arr,background = 0,return_num = True, connectivity = 1) #Binary Image 
    print ("total icebergs",num_feat)
    properties = measure.regionprops(all_labels)
    
    count = 1

    point = 1
    listC = []
    points_lst = []
    coord_area = {}
    with open(root2+str(tile_name)+'.csv', 'w') as csvFile:
        fieldnames = ['Count', 'Area in sq.meters','X','Y', 'Total Area']
        writer = csv.DictWriter(csvFile,fieldnames = fieldnames)
        writer.writeheader()
        for prop in properties:
            area = (prop.area)*100 #10m X 10m spatial resolution
            
            if(area>300):
                
                row,col = prop.centroid #x,y
                x,y = (transform)*(col,row)
                
                writer.writerow({'Count':count, 'Area in sq.meters':area, 'X': x,'Y':y })
                
                count+=1
    
                Area.extend([area])  

        num_bins = 60
    #    print points_lst
        '''
        schema = {'geometry':'MultiPoint',
                  'properties':{'Area':'float',
                                'coordinate':'float'},
                } 
        dst = MultiPoint((points_lst))
        print dst
    #    print mapping(dst)
        print (Area)
        print dict(zip([mapping(dst)],Area))
        print listC
        with fiona.open('mypt.shp','w','ESRI Shapefile',schema,from_epsg(3413)) as c:
            c.write(
                    {
                    'geometry':mapping(dst),
                    'properties':{
                                    'Area':Area, 
    
                                    'coordinate':mapping(dst.coords)},
                                },
                    )
    #    with collection('points2.shp','a','ESRI Shapefile',schema,from_epsg(3413)) as output:
    #                    output.write({
    #                        'geometry':  mapping(dst),
    #                        'properties':{
    #                                'Area':Area, 
    #                                'coordinate':mapping(dst)},
    #                            })
        '''

    '''
    #####################################################
    WRITING THE BINARY IMAGE OF ICEBERGS IDENTIFIED 
    #####################################################
    '''        

    outpath = os.path.join(out_path,output_filename.format(tile_name))
    n, bins, patches = plt.hist(Area, num_bins, facecolor='#3FA7D6', alpha=1,edgecolor='black')
    plt.xlabel('Area of Icebergs (sq. meters)')
    plt.xscale('log')
    plt.xlim(xmin=0)
    plt.ylim(ymin=0)
    plt.grid(True,which='both',axis='both',linestyle='dotted')
    plt.savefig(outpath+'.png',dpi=300)
    plt.show()
    
    
    with ras.open(outpath, 'w', **meta) as outds:
        outds.write(binary_img.astype(np.float32),1)
        outds.close()
    
    
    print ("Time to execute = %s sec" % (time.time() - startTime))