# -*- coding: utf-8 -*-

"""
Created on Sat Jun 22 17:44:47 2019

The MIT License

Copyright (c) 2019 Siddharth Shankar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 


Algorithm: Pre and Post processing along with excerpts from Frost et al, 2016, Canadian Journal of Remote Sensing



#Adding itertools instead for-loops for increasing the speed of loop. Try while as well!
"""





'''
#########################
IMPORT LIBRARIES
#########################
'''

import gdal
import numpy as np
from matplotlib import pyplot as plt
import skimage
from skimage import measure
import csv
import time
import skimage.morphology
from skimage import segmentation
import pandas as pd
import os
from fnmatch import fnmatch
from scipy import ndimage as ndi
import itertools as itr #use to replace for loop
from pyproj import Proj, transform
import rasterio as ras
from shapely.geometry import Point, MultiPoint
from shapely.geometry import mapping
import fiona
from fiona.crs import from_epsg
from fiona import collection
import geopandas as gpd
from collections import OrderedDict
from scipy import ndimage
from skimage.feature import blob_doh,blob_dog,blob_log
from math import sqrt
from rasterio import windows
from itertools import product
import timeit
#import cv2
from skimage.color import rgb2gray
from tqdm import tqdm
import math
from multiprocessing import cpu_count,Pool
import geopandas as gpd
from skimage import exposure
from rasterio.windows import Window
import cv2

'''
#########################
INPUT USER DATA
#########################
'''
#root2= r"/cresis/snfs1/scratch/sshankar/VertexASF/Paper1/Sulak/S1A_IW_GRDH_1SSH_20150716T091139_20150716T091204_006834_00934C_38CF.data/"
#root = r"C:\Research\Fast2DWindowSlider/subset_0_of_S1A_IW_GRDH_1SDH_20180704T083815_20180704T083840_022642_027400_9582_NR_Spk_Cal_TC_NaN.data/"
root2 = r'C:\Research\chunk_read/'
out_path = r'C:\Research\chunk_read\Results/'
pattern = "*.tif" 
output_filename = 'tile_{}-{}.tif'

fileName = []


for _,dirs,files in os.walk(root2,topdown=True): 
    dirs.clear() #excludes the files in subdirectories
    for name in files:   
        if fnmatch(name,pattern):
            fileName.append(name)

print (fileName)


'''
############################
FUNCTIONS FOR IMAGE TO TILES
############################
'''
def get_tiles(ds, width=500, height=500):
    nols, nrows = ds.meta['width'], ds.meta['height']
    offsets = product(range(0, nols, width), range(0, nrows, height))
    big_window = windows.Window(col_off=0, row_off=0, width=nols, height=nrows)
    for col_off, row_off in  offsets:
        window =windows.Window(col_off=col_off, row_off=row_off, width=width, height=height).intersection(big_window)
        transform = windows.transform(window, ds.transform)
        yield window, transform


'''
#########################
READING LIST OF RASTERS
#########################
'''

for sar in fileName:
    #Time setup
    startTime = time.time()
   
    sar_img = root2+sar
    
    read_sar = ras.open(sar_img)
    cols = read_sar.width #number of cols --need to test
    rows = read_sar.height#number of rows --need to test
    print ("rows",rows)
    print ("cols",cols)
    
#    transform = read_sar.transform
    proj = (read_sar.crs)
    
    print(read_sar.profile)
    
#    print (transform)
    
    
    with ras.open(os.path.join(root2, str(fileName.pop()))) as inds:
        tile_width, tile_height = 500, 500

        meta = inds.meta.copy()

        for window, transform in get_tiles(inds):
            print(window,transform)
            meta['transform'] = transform
            meta['width'], meta['height'] = window.width, window.height
            
            img_2Darray = inds.read(1,window=window)
            if (np.count_nonzero(img_2Darray)<100):
                print ("Skipping Image tile:"+str(int(window.col_off)), str(int(window.row_off))+"- has less than 100 data points!")
                continue
            #Histogram equalization added
#            img_m = np.ma.masked_equal(img_2Darray,0)
#            img_m = (img_m - img_m.min())*255/(img_m.max()-img_m.min())
#            img_2Darray = np.ma.filled(img_m,0).astype('float32')
            

            outpath = os.path.join(out_path,output_filename.format(int(window.col_off), int(window.row_off)))
            
            with ras.open(outpath, 'w', **meta) as outds:
                outds.write(img_2Darray.astype(np.float32),1)
                outds.close()
                
                
                print ("Time to execute = %s sec" % (time.time() - startTime))